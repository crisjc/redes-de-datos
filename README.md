# Redes de Computadoras

## Índice
1. [ Principio de funcionamiento de las redes Capitulo 1](#id1)
2. [Seleccion de cable UTP o fibra optica ](#id2)
3. [Cableado estructurado](#id3)
4. [Redes inalambricas](#id4)
5. [Ponchado de cables directo y crusado](#id5)
6. [Configuracion y funcion de los switches](#id6)
7. [Protocolos de comunicacion y puertos](#id7)
8. [Dispostivos hardware and software](#id8)
9. [Capitulo 4 ](#id9)
10. [Preguntas y respuestas](#id10)
11. [Comandos para consola](#id11)


<a name="id1"></a>
## Principio de funcionamiento de las redes
Una red **LAN** o una red de area local es el nombre que se les da las redes para la tranfernecia de datos distancia no mayor a 100 metros con cables par trensado  trasmisores y receptores con el puerto ethernet se conecta a la red.
Modelo OSI se generan protoclos ecanrgados de ordenar y establecer la comunicacion  de sistemas ooperativos y equipos de difeentes arquitectura s y provedores.
Capa fisca: trasmision de la secuencia de bit sin formato por un medio fisico 
capa enlace: idetntificaico logica de los protoclos encapsula lo de la capa superior para enviarlo a la capa de fisica. sube al capa de red con protocolos.
mac, errores tramas lan switches tarjeta  de red nicks.
capa de red: trabajan los router para direccion y enrutar las tramas.

TCP/IP (Orientado a conexion): protocolo tranaja en capa 4 o  trasnposte funcion que lleguen al equipo correcto con las direccion ip de la capa 3 esta direcion se escribe en decimal como por ejemplo 173.194.78.94. es la capa de red y permite conectar estacioens de trabajo y distintos sitemas operativos.

Direccionamiento IP: Cuando se conecta una computadoraa al red la direccio ip puede ser estatica o dinamica(DHCP) o estatica por el administrador de la red conocido como ISP. para redes mucho mas grandes lo que se hace es levantar un sevidor DNS que nos permite asignar nombres especificos para cada red esto es muy utilizado en el internet para acceder a buscadores y paginas en internet.
Para levantar una red LAN minima o pequena se necesita minimo dos computadores conectada a sus tarjetas de red o nicks con una cable utp con el estandar par trenzado, para que se puede estalecer la trasmicion y recepcion de los paquetes enviados entre las dos maquinas.
Se asigna una direccion ip a cada nick si repetirse en cada uno de los sistemas operativos que se tenga o se disponga. Rercordar que se debe especificar la mascara de subred.
ejemplo: 
    ip:192.168.1.1
    mascara:255.255.255.0
### Tipo de redes
redes **wan** redes de area extendida - cubre cuidades 10 a 80 km
redes de area metropolitana **MAN** - redes de rangos de 20 km
redes **LAN** red de area local tipo campos 10Km
**WLAN** redes de wifi no van mas alla de 10k
**DSL** Linea de subscriptor digital que proporcionan acceso internet mediante la trasmision de datos digitales a traves del part trenzado de hilos de cobre convencionales de la red telefonica básica conmuntada 

<a name="id2"></a>
## Seleccion de cable UTP o fibra optica 
tipos de cable de par trenzado.
UTP sin blindaje es bajo costo para altas velocidades puede oacaionr problemas
FTP dispone de pantalla metalica para mejorar las transferencias electromagneticas.
STP es caro robusto y dificil de instalar conector RJ49.

Seleccion de cable utp
categorias: 3 para transportar datos hasta 10mbps
            5 para trasnpostar datos hasta 100mbs
            5e 1000mbps
            6 1gbps compatible con la anteriores categorias
            6a 10gbps
fibra optica nos ayuda a eleminar las perturbacioens eletromagneticas.
fibra optica multimodo: nucleo mayor diametro, varios rayos de luz.
fibra optica monomodo: nucleo menor menor atenuacion por kilometro.
fibra optica cable interior. 

<a name="id3"></a>

## Cableado estructurado
Integra los difrentes servicios de voz datos videso pc impresota tv, debe satisfacer estas necesidades.
Instalacion y mantenimientos asi como administracion de los servicios de datos como voz datos y video.
solucionar fallas de conexion.
node de red, cable UTP, conectores, sal IDF, cableado vertical, cableado  horizontal, sala de parcheo o IDF, asi como SIDE o MDF.
Los nodos son los los componentes se conectan a la red conocidos como jacks. esto se conectan por cable UTP hasta el side de piso, conocido como horizontal de topologia estrella. Un idf o cuarto de parcheo es donde llegan todos los cables UTP en un maso de 24 cables maximo. si un usuario se cambia de oficina lo que se configura de nuevo es unicamente el IDF. 


<a name="id4"></a>
## Redes inalambricas
<a name="id5"></a>
## Ponchado de cables directo y crusado
<a name="id6"></a>
## Configuracion y funcion de los switches
<a name="id7"></a>
## Protocoles de comunicacion y puertos
<a name="id8"></a>
## Dispositovos de hardware y software
<a name="id9"></a>

## Capitulo 4 


Para la parte de aplicacion exiten 4 elemento
* timing 
* trouhput
* sensibilidad
* seguridad

### Tabla de enrutamineto 
Determina el camino end to end a traves de la red 

ATM atraves de cajeros automiaticos 
control de flujo cuando se colapsa los paquetes se empiesas a mandar menos 
frame relay X.25

* Conexion de servicio la parte de red y transporte 
    * entre dos host para red
    * entre dos processo para transporte

### Modelos de servicio

[model-service-red](https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwj6o5_WzK3iAhVMnFkKHYqqDLEQjRx6BAgBEAU&url=https%3A%2F%2Fwww.cs.umd.edu%2F~shankar%2F417-F01%2FSlides%2Fchapter4a-aus%2Fsld007.htm&psig=AOvVaw1t78Z_pLZhhX5Zp7BnkpmJ&ust=1558561373842162)


### Red ATM 
se realizar dos procesos 
* forwarding 
* Routing

### Arquitectura de un router
El protocolo de puerta de enlace de frontera (BGP)
Routing Information Protocol (RIP)


### Tipos de tramas 

para diferentes tipos 





<a name="id10"></a>
## Terminos tecnicos en redes de computadores

**Protocolo ARP**: Para buscar la maquina

**ADLS**:es un dispositivo que permite conectar al mismo tiempo uno o varios equipos o incluso una o varias redes de área local (LAN).

**DNS**: Sistema de nombres de dominio

**Vlan**: es un método para crear redes lógicas independientes dentro de una misma red física.

**Trama Vlan**:El protocolo 802.1Q propone añadir 4 bytes al encabezado Ethernet original en lugar de encapsular la trama original

**DHCP**: funciona sobre un servidor central (servidor, estación de trabajo o incluso un PC) el cual asigna direcciones IP a otras máquinas de la red. Este protocolo puede entregar información IP en una LAN o entre varias VLAN. ... Un servidor DHSC (DHCP Server) es un equipo en una red que está corriendo un servicio DHCP.

**CIDR**:Fija en la asignacion de host al subnetting

**VLSM**:Variable en la asignacion de host al subnetting

**Ad hoc**: es el modo más sencillo para el armado de una red.

**ScID** es un identifiador de las red inalambrica

**wifi**: 802.11 estandar par wifi

**Canales WIFI**: el 1 6 11 son los que no se solapan garantizando una buena senial si ningun tipo de interferencia

**codigo de colores** 
![codigo colores](https://files.123inventatuweb.com/acens56695/image/codigocoloresutp.jpg)

**Redes ipv4 e ipv6?**
<a name="id11"></a>
## Comandos para consola


            
## Redes Sociales
<a href="https://twitter.com/crisjc8" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @crisjc8 </a><br>
<a href="https://linkedin.com/in/cristhian-jumbo-748934180/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Cristhian Jumbo</a><br>
<a href="https://www.instagram.com/crisjc6/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> crisjc6</a><br>
